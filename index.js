const express = require('express');
const fs = require('fs');
const path = require('path');
const morgan = require('morgan');

const port = 8080;

const app = express();
app.use(express.json());
app.use(morgan('tiny'));

const arrayFiles = [];
const folder = path.join(__dirname, '/files');

fs.readdir(folder, (err, files) => {
    files.forEach((file) => {
        console.log(file);
        arrayFiles.push(file);
    });
});

app.post('/files', (req, res) => {
    try {
        const {filename, content} = req.body;

        const regexp = /\.(?:log|txt|json|yaml|xml|js)$/g;
        if (filename && content) {
            if (regexp.test(filename)) {
                fs.writeFile(path.join(folder, filename), content, function () {
                    console.log('File created successfully');
                });

                res.status(200).send({message: 'File created successfully'});
            } else {
                res.status(400).send({message: 'Wrong extension'});
            }
        } else {
            if (!content) {
                res.status(400).send({message: "Please specify 'content' parameter"});
            }
            if (!filename) {
                res.status(400).send({message: "Please specify 'filename' parameter"});
            }
        }
    } catch (e) {
        res.status(500).send({message: `${e} Error`});
    }
});

app.get('/files', (req, res) => {
    try {
        if (arrayFiles.length >= 0) {
            res.status(200).send({
                message: 'Success',
                files: arrayFiles
            });
        } else {
            res.status(400).send({message: 'Client error'});
        }
    } catch (e) {
        res.status(500).send({message: `${e} Error`});
    }
});

app.get('/files/:filename', (req, res) => {
    try {
        const filename = req.params.filename;

        if (filename) {
            const result = arrayFiles.find((file) => file === filename);
            if (result) {
                const uploadedDate = fs.statSync(path.join(folder, result)).birthtime;
                const content = fs.readFileSync(path.join(folder, result)).toString();
                const extension = path.extname(result).split('.')[1];

                res.status(200).send({
                    message: 'Success',
                    filename,
                    content,
                    extension,
                    uploadedDate
                });
            } else {
                res.status(400).send({message: `No file with ${filename} filename found`});
            }
        }
    } catch (e) {
        res.status(500).send({message: `${e} Error`});
    }
});

app.put('/files/:filename', (req, res) => {
    try {
        const filename = req.params.filename;
        const {filename: newFilename, content: newContent} = req.body;
        if (filename) {
            const result = arrayFiles.find((file) => file === filename);
            if (result) {
                if (newContent) {
                    const content = fs.readFileSync(path.join(folder, result)).toString();
                    if (content !== newContent) {
                        fs.writeFileSync(path.join(folder, result), newContent);
                    }
                }

                if (newFilename) {
                    if (filename !== newFilename) {
                        fs.renameSync(
                            path.join(folder, filename),
                            path.join(folder, newFilename)
                        );
                    }
                }

                if (!newFilename && !newContent) {
                    res.status(400).send({message: 'Enter new filename or new content'});
                }

                res.status(200).send({message: 'File updated successfully'});
            } else {
                res.status(400).send({message: `No file with ${filename} filename found`});
            }
        }
    } catch (e) {
        res.status(500).send({message: `${e} Error`});
    }
});

app.delete('/files/:filename', (req, res) => {
    try {
        const filename = req.params.filename;

        if (filename) {
            const result = arrayFiles.find((file) => file === filename);

            if (result) {
                fs.unlink(path.join(folder, result), function () {
                    console.log('File deleted successfully');
                });
                res.status(200).send({message: 'File deleted successfully'});
            } else {
                res.status(400).send({message: `No file with ${filename} filename found`});
            }
        }
    } catch (e) {
        res.status(500).send({message: `${e} Error`});
    }
});


app.listen(3000, () => {
    console.log(`Server started on ${port}`);
});

